# Copyright (c) Markus Binsteiner, 2019. All rights reserved.
import sys

from python import entrypoint
import logging
from anyio import run

from retailiate_core.core import Context


def main(args):

    try:
        context = Context()

        result = await entrypoint.workflow(args=args, context=context)

        return result
    except (Exception) as e:
        logging.error(e)
        raise e


if __name__ == '__main__':

    run(sys.argv[1:])

