import logging
from typing import List

from retailiate_core.core import Retailiate, Context

retailiate = Retailiate()

log = logging.getLogger("{{ cookiecutter.project_name }}")

async def workflow(args: List[str], context: Context):

    """handle a request to the function
    Args:
        req: request body
    """

    return args
